from tkinter import *
from tkinter.messagebox import *
import serial
import sys

def recevoir():
    PORT = "COM5"
    BAUD = 115200
    s = serial.Serial(PORT)
    s.baudrate = BAUD
    s.parity  = serial.PARITY_NONE
    s.databits = serial.EIGHTBITS
    s.stopbits = serial.STOPBITS_ONE

    while True:
        val = s.readline()
        val = val.decode('utf-8')
        val = val.strip()
        return(val)

    s.close()

joueur = "Au tour du joueur 1 | X"

def Choix(case2):
    global a,C1,C2,C3,C1RC,C1R,C2RC,C2R,C3RC,C3R,L1RC,L1R,L2RC,L2R,L3RC,L3R,L1,L2,L3,joueur
    #Quand a = 1 on met une croix (joueur 1)
    case= int(case2)
    if a==1:
        if case==1:
            if C1[0]==2:
                a=0
                C1[0]=1
                L1[0]=1
                Croix(50,50)
                joueur=('Au tour du joueur 2  | O')
            else :
                showinfo(title='Erreur !',message='La case est déjà occupée')
        if case==2:
            if C1[1]==2:
                a=0
                C1[1]=1
                L2[0]=1
                Croix(50,150)
                joueur=('Au tour du joueur 2  | O')
            else :
                showinfo(title='Erreur !',message='La case est déjà occupée')
        if case==3:
            if C1[2]==2:
                a=0
                C1[2]=1
                L3[0]=1
                Croix(50,250)
                joueur=('Au tour du joueur 2  | O')
            else :
                showinfo(title='Erreur !',message='La case est déjà occupée')
        if case==4:
            if C2[0]==3:
                a=0
                C2[0]=1
                L1[1]=1
                Croix(150,50)
                joueur=('Au tour du joueur 2  | O')
            else :
                showinfo(title='Erreur !',message='La case est déjà occupée')
        if case==5:
            if C2[1]==3:
                a=0
                C2[1]=1
                L2[1]=1
                Croix(150,150)
                joueur=('Au tour du joueur 2  | O')
            else :
                showinfo(title='Erreur !',message='La case est déjà occupée')
        if case==6:
            if C2[2]==3:
                a=0
                C2[2]=1
                L3[1]=1
                Croix(150,250)
                joueur=('Au tour du joueur 2  | O')
            else :
                showinfo(title='Erreur !',message='La case est déjà occupée')
        if case==7:
            if C3[0]==4:
                a=0
                C3[0]=1
                L1[2]=1
                Croix(250,50)
                joueur=('Au tour du joueur 2  | O')
            else :
                showinfo(title='Erreur !',message='La case est déjà occupée')
        if case==8:
            if C3[1]==4:
                a=0
                C3[1]=1
                L2[2]=1
                Croix(250,150)
                joueur=('Au tour du joueur 2  | O')
            else :
                showinfo(title='Erreur !',message='La case est déjà occupée')
        if case==9:
            if C3[2]==4:
                a=0
                C3[2]=1
                L3[2]=1
                Croix(250,250)
                joueur=('Au tour du joueur 2  | O')
            else :
                showinfo(title='Erreur !',message='La case est déjà occupée')

    #Quand a = 0 on met un rond (joueur 2)
    else:
        if case==1:
            if C1[0]==2:
                a=1
                C1[0]=0
                L1[0]=0
                Rond(50,50)
                joueur=('Au tour du joueur 1  | X')
            else :
                showinfo(title='Erreur !',message='La case est déjà occupée')
        if case==2:
            if C1[1]==2:
                a=1
                C1[1]=0
                L2[0]=0
                Rond(50,150)
                joueur=('Au tour du joueur 1  | X')
            else :
                showinfo(title='Erreur !',message='La case est déjà occupée')
        if case==3:
            if C1[2]==2:
                a=1
                C1[2]=0
                L3[0]=0
                Rond(50,250)
                joueur=('Au tour du joueur 1  | X')
            else :
                showinfo(title='Erreur !',message='La case est déjà occupée')

        if case==4:
            if C2[0]==3:
                a=1
                C2[0]=0
                L1[1]=0
                Rond(150,50)
                joueur=('Au tour du joueur 1  | X')
            else :
                showinfo(title='Erreur !',message='La case est déjà occupée')
        if case==5:
            if C2[1]==3:
                a=1
                C2[1]=0
                L2[1]=0
                Rond(150,150)
                joueur=('Au tour du joueur 1  | X')
            else :
                showinfo(title='Erreur !',message='La case est déjà occupée')
        if case==6:
            if C2[2]==3:
                a=1
                C2[2]=0
                L3[1]=0
                Rond(150,250)
                joueur=('Au tour du joueur 1  | X')
            else :
                showinfo(title='Erreur !',message='La case est déjà occupée')

        if case==7:
            if C3[0]==4:
                a=1
                C3[0]=0
                L1[2]=0
                Rond(250,50)
                joueur=('Au tour du joueur 1  | X')
            else :
                showinfo(title='Erreur !',message='La case est déjà occupée')
        if case==8:
            if C3[1]==4:
                a=1
                C3[1]=0
                L2[2]=0
                Rond(250,150)
                joueur=('Au tour du joueur 1  | X')
            else :
                showinfo(title='Erreur !',message='La case est déjà occupée')
        if case==9:
            if C3[2]==4:
                a=1
                C3[2]=0
                L3[2]=0
                Rond(250,250)
                joueur=('Au tour du joueur 1  | X')
            else :
                showinfo(title='Erreur !',message='La case est déjà occupée')
    return(joueur)

#Fonction pour mettre un rond
def Rond(x1,y1):
    global C1,C2,C3
    Canevas.create_oval(x1-45,y1-45,x1+45,y1+45)
    Verif()

#Fonction pour mettre une croix
def Croix(x1,y1):
    global C1,C2,C3
    Canevas.create_line(x1-45,y1-45,x1+45,y1+45)
    Canevas.create_line(x1+45,y1-45,x1-45,y1+45)
    Verif()

#Fonction pour compter les points
def Verif():
    global C1, C2,C3,C1RC,C1R,C2RC,C2R,C3RC,C3R,L1RC,L1R,L2RC,L2R,L3RC,L3R,L1,L2,L3
    C1RC = C1.count(1)
    C1R = C1.count(0)
    C2RC = C2.count(1)
    C2R = C2.count(0)
    C3RC = C3.count(1)
    C3R = C3.count(0)

    L1RC = L1.count(1)
    L1R = L1.count(0)
    L2RC = L2.count(1)
    L2R = L2.count(0)
    L3RC = L3.count(1)
    L3R = L3.count(0)

#Conditions pour le gain du jeu et affichage du vainqueur
    if C1RC == 3:
        showinfo(title='Félicitations',message='Joueur 1 a gagné !')
        Mafenetre.destroy()
    if C1R == 3:
        showinfo(title='Félicitations',message='Joueur 2 a gagné !')
        Mafenetre.destroy()
    if C2RC == 3:
        showinfo(title='Félicitations',message='Joueur 1 a gagné !')
        Mafenetre.destroy()
    if C2R == 3:
        showinfo(title='Félicitations',message='Joueur 2 a gagné !')
        Mafenetre.destroy()
    if C3RC == 3:
        showinfo(title='Félicitations',message='Joueur 1 a gagné !')
        Mafenetre.destroy()
    if C3R == 3:
        showinfo(title='Félicitations',message='Joueur 2 a gagné !')
        Mafenetre.destroy()

    if C1[0]==1 and C2[1]==1 and C3[2]==1:
        showinfo(title='Félicitations',message='Joueur 1 a gagné !')
        Mafenetre.destroy()
    if C1[2]==1 and C2[1]==1 and C3[0]==1:
        showinfo(title='Félicitations',message='Joueur 1 a gagné !')
        Mafenetre.destroy()

    if C1[0]==0 and C2[1]==0 and C3[2]==0:
        showinfo(title='Félicitations',message='Joueur 2 a gagné !')
        Mafenetre.destroy()
    if C1[2]==0 and C2[1]==0 and C3[0]==0:
        showinfo(title='Félicitations',message='Joueur 2 a gagné !')
        Mafenetre.destroy()

    if L1RC == 3:
        showinfo(title='Félicitations',message='Joueur 1 a gagné !')
        Mafenetre.destroy()
    if L1R == 3:
        showinfo(title='Félicitations',message='Joueur 2 a gagné !')
        Mafenetre.destroy()
    if L2RC == 3:
        showinfo(title='Félicitations',message='Joueur 1 a gagné !')
        Mafenetre.destroy()
    if L2R == 3:
        showinfo(title='Félicitations',message='Joueur 2 a gagné !')
        Mafenetre.destroy()
    if L3RC == 3:
        showinfo(title='Félicitations',message='Joueur 1 a gagné !')
        Mafenetre.destroy()
    if L3R == 3:
        showinfo(title='Félicitations',message='Joueur 2 a gagné !')
        Mafenetre.destroy()


#Initialisation des colonnes et des lignes

C1RC,C1R,C2RC,C2R,C3RC,C3R,L1RC,L1R,L2RC,L2R,L3RC,L3R = 0,0,0,0,0,0,0,0,0,0,0,0

a=1

C1=[2,2,2]
L1=[2,2,2]

C2=[3,3,3]
L2=[3,3,3]

C3=[4,4,4]
L3=[4,4,4]

#Partie graphique
Mafenetre = Tk()
Mafenetre.title("Morpion")

Largeur = 300
Hauteur = 300

Canevas = Canvas(Mafenetre, width = Largeur, height =Hauteur, bg ="white")

Canevas.bind("<Button-1>", Choix)
Canevas.pack(padx =5, pady =5)
Canevas.create_line(0,100,300,100,fill="black",width=4)

Canevas.create_line(0,200,300,200,fill="black",width=4)

Canevas.create_line(100,300,300,-100000,fill="black",width=4)

Canevas.create_line(200,300,300,-100000,fill="black",width=4)

label_turn = Label(Mafenetre, text=joueur)
label_turn.pack(side=RIGHT)
label = Label(Mafenetre, text="Joueur 1 | X")

Button(Mafenetre, text ="Quitter", command = Mafenetre.destroy).pack(side=LEFT,padx=5,pady=5)

#Ici on a la partie réception qui récupère les actions effectuées sur le microbit
def gestion_reception():
    case = recevoir()
    print(case)
    joueur = Choix(case)
    label_turn['text'] = joueur
    Canevas.after(100, gestion_reception)

Canevas.after(100, gestion_reception)

Mafenetre.mainloop()



