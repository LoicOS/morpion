# Ecrit ton programme ici ;-)
from microbit import *

val = 1

while True:
    display.show(val)
    if button_a.get_presses():
        val = val+1
        if val == 10:
            val = 1
    if button_b.get_presses():
        message = "%s\n" % val
        uart.write(message)
