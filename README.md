# Morpion

Jeu du morpion utilisant les boutons de la microbit pour se déplacer dans la grille et sélectionner une case.

Règles du jeu :

Le gagnant est le premier joueur à former une ligne de "X" ou de "O".
Le joueur 1 utilise la croix, le joueur 2 utilise le rond.

1 - Faites défiler les chiffres de 1 à 9 à l'aide du bouton gauche de la microbit.
Chaque chiffre correpsond à une case de la grille tel que :

1|4|7
2|5|8
3|6|9

2 - Validez la case souhaitée à l'aide du bouton droit.

3 - C'est ensuite au tour du joueur 2, répéter les opérations 1 et 2.

Que le meilleur gagne !!